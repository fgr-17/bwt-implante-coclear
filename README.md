Proyecto Analisis de eficiencia de un algoritmo de Implante Coclear mediante RNA

Autor :         ROUX, Federico G. (rouxfederico@gmail.com)

13-12-2014 :

        MODIFICACIONES :

        > Inicializacion del repo. Ya esta resuelta la base de datos y su
          documentacion. Se trabaja sobre el procesamiento de datos y la
          funcion para analizar los sonidos de pronunciaciones en ventanas de
          un determinado largo (4ms segun el paper de trabajo).


        PENDIENTE :


		06/12/2014 :

		- Informe 2 : ondita wavelet munich y bandas criticas: escribir teoría de bandas críticas
		- centrar la frecuencia de la oscilación de la wavelet al centro, para multiplicar por la envolvente y quede valor medio más cerca de cero
		- si la envolvente es muy estrecha, no entran suficiente cantidad de muestras de la oscilación y la f0 del filtro final no se puede ajustar más

		11/12/2014 :

		- hace falta agregar los sonidos sordos? los de "closure"?
		- arreglar el comienzo y fin de cada sonido. No está bien alineado
		- separar los scripts de clasificacion de los sonidos, cálculo de la wavelet de cada uno y cálculo de la red. En cada uno guardar los resultados en archivos y levantarlos con el otro para ahorrar tiempo.

14/12/2014 : 

	Pendiente :

	> Variar la cantidad de muestras superpuestas en qué influye en los resultados ? 
	> evaluar compresion o tecnicas de audio (eq.) para ver si mejora la respuesta. Filtrar el ruido? agregar ruido?
	> si el N_dsp es el mismo en todas las escalas, el tamaño de la salida en cada escala no varía?
	> hago la cantidad de canales a procesar variable?
	> me guardo el número de escala elegida, pero después qué hago con eso?
	> mejorar el tema del path de búsqueda de archivos

17/12/2014 :

	Modificaciones :

	> Resuelto todo el manejo de archivos para las distintas etapas del trabajo

26.01.2014 :

        > Finalizado informe de avance 2.
        > Corregidos algunos errores de etiquetado y manejo de archivos.

27.01.2015 :

		> Corregido etiquetas se guardan y leen como "double" para el caso de la transición entre dos fonemas
		> Corregida funcion para leer archivo de libro.

hasta 03/02/2015 :

	Modificaciones :

	> Reproducir_Entrada se puede usar para un sonido, una serie o todo el archivo completo :)
	> Los datos de entrada de la red se acomodaron con la función Libro_a_Array ()
	> Red entrenada para un solo sonido /dV/ con error pero se probó como ingresar los datos de entrada
	> Se agregó archivos de clasificación de sonidos de la base TIMTI para examinar todos los fonemas (ver doc. de texto)
    > Script para entrenar red escribe log y plots de salida en archivos.

hasta 10/02/2015 :

	Modificaciones :

	> Corregido error de ordenamiento de escalas
	> Corregido error de calculo de la CWT
	> Corregido error de segmentacion y CWT : primero calculo CWT y luego segmento
	> Agregado guardar tr y net en el entrenamiento de la red 

hasta 11/02/2015 :

	> Se hicieron relativos todos los paths de archivos 