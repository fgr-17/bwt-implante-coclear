%% ========================================================================
%
%>  @file Abrir_Pronunciacion_Procesada.m
%>  @function [y] = Guardar_Pronunciacion_Procesada (libro_proc) 
%>  @brief Recibe un libro de procesamiento con el sonido procesado por 
%>  @brief CWT, del que solo se separaron una serie de escalas (en el paper
%>  @brief son 6), procesados mediante ventanas de 4ms solapando una cierta
%>  @brief cantidad de muestras.
%
%>  @author  ROUX, Federico G. (rouxfederico@gmail.com)
%
%>  @company    Departamento de Ing.Electronica.
%>  @company    Facultad Buenos Aires
%>  @company    Universidad Tecnologica Nacional
%% ========================================================================
function [libro_proc, nombre_fonema] = Abrir_Pronunciacion_Procesada (archivo)

% La asocaci�n con el archivo original de TIMIT conviene guardarlo en el
% nombre. O mejor agregar un campo con esa informaci�n?
% GUardo un arhcivo con los nombres de archivos separados por criterio de
% clasificaci�n?
fid = fopen(archivo, 'rb');                                                 % abro el archivo que voy a escribir 
nombre_fonema = fgets(fid);                                                 % Guardo nombre de fonema para referencia

cant_etiquetas = fread(fid, 1, 'int16');                                    % Guardo la cantidad de etiquetas
cant_paginas = fread(fid, 1, 'int16');                                      % Cantidad de p�ginas a procesar
cant_escalas_elegidas = fread(fid, 1, 'int16');                             % Cantidad de escalas por p�gina
cant_muestras_escala = fread(fid, 1, 'int16');                              % Cantidad de muestras por escala
N_dsp = fread(fid, 1,'int16');                                                % Cantidad de muestras de procesamiento
N_sup = fread(fid, 1,'int16');                                                % Cantidad de muestras superpuestas 

etiquetas = fread(fid, cant_etiquetas, 'double');                            % Guardo el array de etiquetas

libro = zeros(cant_paginas, cant_escalas_elegidas, cant_muestras_escala);

for pag = 1 : cant_paginas   
    for escala = 1 : cant_escalas_elegidas       
            libro(pag, escala, :) = fread(fid, cant_muestras_escala, 'double');           % Guardo toda la informaci�n procesada
    end
end

fclose (fid);                                                               % Cierro archivo

libro_proc.libro = libro;
libro_proc.etiq = etiquetas;
libro_proc.n_dsp = N_dsp;
libro_proc.n_sup = N_sup;
libro_proc.n_pag = size(libro, 1);
libro_proc.n_esc = size(libro, 2);
end