%% ========================================================================
%
%>  @file Procesar_Pronunciacion.m
%>  @function [y] = Procesar_Pronunciacion 
%>  @brief version propia del algoritmo de CWT, haciendo la convolucion de 
%>  @brief la senal con la integral de la wavelet desplazada y escalada y 
%>  @brief luego diferenciando esta seal (con el m�todo contrario usado 
%>  @brief para integrar).
%
%>  @author  ROUX, Federico G. (rouxfederico@gmail.com)
%
%>  @company    Profesor: Ing. BRUNO, Julian S.
%>  @company    Procesamiento Digital de Se�ales(PDS)
%>  @company    Departamento de Ing.Electronica.
%>  @company    Facultad Buenos Aires
%>  @company    Universidad Tecnologica Nacional
%% ========================================================================

% El an�lisis se hace en buffer de tama�o de 4ms, como est� en el paper, y
% se solapan algunas pocas muestras (15 de las 64 que tiene el buffer) para
% procesar de una manera m�s cont�nua. 

function libro_procesado = Procesar_Pronunciacion_2 (snd, archivo_wavelet)

Fs_sonido = snd.Fs;                                                         % Frecuencia de muestreo del sonido a procesar

cant_fonemas = length(snd.s);                                               % Cantidad de fonemas contenidos

sonido = 0;                                                                 % Inicializo array de sonidos
separadores_snd = zeros(cant_fonemas, 1);                                   % Inicializo array de separadores

L = zeros(1, cant_fonemas);

for i_fonema = 1 : cant_fonemas                                             % Recorro todos los fonemas de la estructura
   sonido = [sonido ; snd.s{i_fonema}];                                     % Guardo los sonidos todos continuados
   separadores_snd (i_fonema) = length(snd.s{i_fonema});                    % Guardo el separador asociado para indicar donde termina
   L (i_fonema) = length(snd.s{i_fonema});
   
end



separadores_snd = cumsum(separadores_snd);                                  % pongo las referencias de inicio del fono sobre el final del anterior

%L = length(sonido);                                                         % Cantidad total de muestras de la frase

[z1, z2, N, z3, z4, cant_escalas, a, w] = Abrir_Wavelet_Archivo (archivo_wavelet);

ventana_dsp = 4e-3;                                                         % Ventana de procesamiento : 4ms
N_dsp = fix(ventana_dsp * Fs_sonido);                                       % cantidad de muestras a analizar por vez

ESCALAS_ELEGIDAS = 6;                                                       % Cantidad de escalas con mayor RMS elegidas por umbral 
N_SUP_PAPER = 10;                                                           % cantidad de muestras superpuestas en el paper
FS_PAPER = 11025;                                                           % Fs del paper como referencia
N_sup = fix(N_SUP_PAPER *(Fs_sonido / FS_PAPER));                           % Cantidad de muestras superpuestas reales

if(ESCALAS_ELEGIDAS >= cant_escalas)                                        % 
    error('Error en la cantidad de escalas elegidas');
end

if((N_dsp > N)||(N_sup >= N))
    error('Error en la cantidad de muestras de superposici�n o de procesamiento');
end

N_avance = N_dsp - N_sup;                                                   % cantidad de muestras que avanzo 

cant_pag = floor((L - N_avance) ./ N_avance);                                 % Cantidad de iteraciones (p�g. de procesamiento)

cant_pag_total = sum(cant_pag);

y = zeros(cant_pag_total, ESCALAS_ELEGIDAS, N_dsp);                               % Inicializo array de salida

etiquetas = zeros(cant_pag_total, 1);                                             % Inicializo cantidad de etiquetas


pag_final = 1;
for i_fonema = 1 : cant_fonemas

    media_sonido = mean(snd.s{i_fonema});
    snd.s{i_fonema} = snd.s{i_fonema} - media_sonido;                                             % media cero

    max_sonido = max(snd.s{i_fonema});
    min_sonido = min(snd.s{i_fonema});
    max_abs_sonido = max(abs(max_sonido), abs(min_sonido));
	snd.s{i_fonema} = snd.s{i_fonema} / max_abs_sonido;                                           % Rango entre +1 y -1
   
    n_ini = 1;                                                                  % Inicializo nro. de muestra de origen de la ventana
    for pag = 1 : cant_pag(i_fonema)                                                      % Recorro todas las p�ginas que voy a generar
    
        
        x = snd.s{i_fonema}(n_ini:(n_ini + N_dsp - 1));                              % porcion de sonido a procesar

        


        y_cwt = Calcular_CWT (x, a, w);                                         % proceso la porci�n de se�al

        K = size(y_cwt, 1);                                                     % cantidad de muestras de la ondita
        y_rms = sqrt(sum((y_cwt .^2), 2)/ K);                                   % calculo el vector rms
        [y_rms_ord esc_ord]= sort(y_rms);                                       % ordeno para sacar los primeros 6

        escalas_seleccionadas = esc_ord (1:ESCALAS_ELEGIDAS);                   % Guardo las primeras 6 escalas
        esc_elegidas_ord = sort(escalas_seleccionadas);                         % Vuelvo a ordenar para respetar los valores de freq.
    
        y(pag_final, :, :) = y_cwt(esc_elegidas_ord, :);                              % Genero p�ginas de procesamiento para entrenar la RNA
        etiquetas(pag_final) = snd.valores (i_fonema);
        pag_final = pag_final + 1;
        n_ini = n_ini + N_avance;
    end
    
end
libro_procesado.libro = y;
libro_procesado.etiq = etiquetas;
libro_procesado.n_dsp = N_dsp;
libro_procesado.n_sup = N_sup;

end