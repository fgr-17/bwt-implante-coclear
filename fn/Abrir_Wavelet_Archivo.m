%% ========================================================================
%
%>  @file Guardar_Wavelet_Archivo.m
%>  @function Guardar_Wavelet_Archivo (encabezado, a, w)
%>  @brief Guarda una wavelet con el encabezado con informaci�n de su
%>  @brief s�ntesis, las escalas que se eligen para un cierto prop�sito
%>  @brief (por ejemplo, las bandas ERB a analizar) y luego las muestras de
%>  @brief la wavelet sintetizada
%
%>  @author  ROUX, Federico G. (rouxfederico@gmail.com)
%
%>  @company    Profesor: Ing. BRUNO, Julian S.
%>  @company    Procesamiento Digital de Se�ales(PDS)
%>  @company    Departamento de Ing.Electronica.
%>  @company    Facultad Buenos Aires
%>  @company    Universidad Tecnologica Nacional
%% ========================================================================

function [tini tfin N Fs f0_Hz cant_escalas a w] = Abrir_Wavelet_Archivo (archivo_dat)

fid2 = fopen (archivo_dat, 'rb');

% Formato del archivo de la wavelet :
% <<tini>>
% <<tfin>>
% <<N>>
% <<Fs>>
% <<f0_Hz>>
% <<cant_escalas>>
% - escalas en 'float'
% - muestras de la wavelet en 'float'

% Escribo archivo de salida con informaci�n de la wavelet :
encabezado = fscanf(fid2, '%f\n%f\n%f\n%f\n%f\n%f\n');            % Formato del encabezado con info. de la wavelet
% [tini tfin N Fs f0_Hz cant_escalas]           
tini = encabezado (1);
tfin = encabezado (2);
N = encabezado (3);
Fs = encabezado (4);
f0_Hz = encabezado (5);
cant_escalas = encabezado (6);

a = fread (fid2, cant_escalas, 'float');                                    % Leo todas las escalas guardadas

w = fread (fid2, inf, 'double');                                            % Guardo las muestras de la wavelet 

fclose(fid2);                                                               % Cierro archivo de wavelet

end