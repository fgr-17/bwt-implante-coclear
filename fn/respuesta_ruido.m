
%% ========================================================================
%
%>  @file   respuesta_ruido.m
%
%>  @author Roux, Federico G. (rouxfederico@gmail.com)
%>  @company    UTN.BA 2014
%% ========================================================================

%% ==================== Inicialización del script ========================= 


clear all;
fclose all;
close all 
clc;
disp(' ==============================================================');
disp(' -              PREPARACION DEL SET DE DATOS                  -');
disp(' ==============================================================');
fprintf(1, '\n\n');
%% Abro archivo de biblioteca :7

path_propio = mfilename('fullpath');                                        % Obtengo el path propio sin extension
path_desde_base = 'fn\respuesta_ruido';                               % Path fijo del archivo en la base
path_base = strrep(path_propio, path_desde_base, '');                       % Extraigo path de la base


path_bib = [path_base 'archivos de salida\CWT_SNR_02dB\'];                              % Ruta donde guardo archivos de salida
archivo_bib = [path_bib 'FAKS0.bib'];                                       % lista de archivos con la pronunciacion dV
fid_bib = fopen (archivo_bib, 'rt');                                        % Abro para guardar texto (nombres de archivos)

i_libro = 1;

entrada = null(1);
target = null(1);

textprogressbar('Abriendo libros procesados           ');
%% Extraigo archivos de la biblioteca :


while(not(feof(fid_bib)))
    
    textprogressbar(10);
    archivo_lbr = fgets(fid_bib);                                                           % Leo una linea : corresponde a un archivo de libro
    archivo_lbr = [path_base archivo_lbr(1:(length(archivo_lbr) - 1))];                                 % Le quito el caracter de ENTER
    [libro_proc{i_libro}, nombre_fonema] = Abrir_Pronunciacion_Procesada (archivo_lbr);     % Abro el archivo y extraigo la información
    
    textprogressbar(40);
    array_libro = Libro_a_Array (libro_proc{i_libro});
    
    entrada = horzcat(entrada, array_libro);
    target = horzcat(target, libro_proc{i_libro}.etiq');
    i_libro = i_libro + 1;                                                                  % paso al siguiente libro
    textprogressbar(100);
end
fclose(fid_bib);
textprogressbar('Finalizado');

%% EVALUO RED CWT :
 
open net_cwt_75.mat
net_cwt = ans.net;

open tr_cwt_75.mat
tr_cwt = ans.tr;

output_cwt = net_cwt(entrada);                                                      % Simulo la salida para el entrenamiento hecho
output_train_cwt = output_cwt(tr_cwt.trainInd);                                         % Salida del set de entrenamiento
output_val_cwt = output_cwt(tr_cwt.valInd);                                             % Salida del set de validacion
output_test_cwt = output_cwt(tr_cwt.testInd);                                           % Salida del set de testeo

target_train_cwt = target(tr_cwt.trainInd);                                         % Targets del set de entrenamiento
target_val_cwt = target(tr_cwt.valInd);                                             % Targets del set de validacion
target_test_cwt = target(tr_cwt.testInd);                                           % Targets del set de testeo

regresion_train_cwt = regression(target_train_cwt, output_train_cwt);
regresion_val_cwt = regression(target_val_cwt, output_val_cwt);
regresion_test_cwt = regression(target_test_cwt, output_test_cwt);
regresion_total_cwt = regression(target, output_cwt);

fprintf(1, '\t> Respuesta de la red CWT para SNR = 0.2 dB :\n');
fprintf(1, '\t\t - Regresion Entrenamiento : %f\n', regresion_train_cwt);
fprintf(1, '\t\t - Regresion Validacion : %f\n', regresion_val_cwt);
fprintf(1, '\t\t - Regresion Test : %f\n', regresion_test_cwt);
fprintf(1, '\t\t - Regresion Total : %f\n', regresion_total_cwt);





