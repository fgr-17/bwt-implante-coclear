%% ========================================================================
%
%>  @file script_Graficar_Senal_Fonema.m
%
%>  @brief  extraigo los fonemas que necesito de la base de datos de TIMIT
%>  @brief  para despu�s procesarlos con CWT y guardarlos en archivos
%
%>  @author Roux, Federico G. (rouxfederico@gmail.com)
%>  @company    UTN.BA 2014
%% ========================================================================

clear all;
close all;

imagenesPath = 'K:/IIA/imagenes/';

%% Abro nueva entrada :

faks0_sa1 = Extraer_Fonemas('\FAKS0\SA1');                                  % Entrada : 0 63488 She had your dark suit in greasy wash water all year.

%% Divido los fonemas a graficar en diferentes se�ales :

fonema_sh = faks0_sa1.fonemas.muestras_audio{2};
fonema_e = faks0_sa1.fonemas.muestras_audio{3};
fonema_sil = faks0_sa1.fonemas.muestras_audio{4}; 
fonema_h = faks0_sa1.fonemas.muestras_audio{5}; 

%% Grafico todos los resultados que quiero mostrar
fonemas_temporal = figure();                                                             % Genero la figura para guardar archivo
subplot(4, 1, 1);
Grafico_Temporal(fonema_sh, 16000, 0, '/sh/', 't[s]', 'y', '-r', 0, 'Fonemas');
subplot(4, 1, 2);
Grafico_Temporal(fonema_e, 16000, 0, '/e/', 't[s]', 'y', '-r', 0, 'Fonemas');
subplot(4, 1, 3);
Grafico_Temporal(fonema_sil, 16000, 0, '/h/', 't[s]', 'y', '-r', 0, 'Fonemas');
subplot(4, 1, 4);
Grafico_Temporal(fonema_h, 16000, 0, '/a/', 't[s]', 'y', '-r', 0, 'Fonemas');

%% Guardo la imagen que quiero mostrar

saveas(fonemas_temporal,[imagenesPath 'fonemas_temporal.png']);
