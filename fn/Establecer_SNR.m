function y = Establecer_SNR (x, SNR)

Px = sum(x.^2);
xN = length(x);
ruido = rand(xN, 1);

Prm = sum(ruido.^2);

k = sqrt((Px / Prm) / (10^(SNR/20)));
ruido_final = k*ruido;

y = x + ruido_final;


end