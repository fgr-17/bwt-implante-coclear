%%=========================================================================================================================================
%
%>  @file           my_Morlet_Munich.m
%
%>  @brief          Genero la ondita Morlet Munich a partir de par�metros
%
%>  @paramin        li      L�mite inferior del soporte
%>  @paramin        ls      L�mite superior del soporte
%>  @paramin        N       Cantidad de muestras a generar
%
%>  @author        Roux, Federico G.(rouxfederico@gmail.com)
%>  @company       RT-DSP UTN.BA
%==========================================================================================================================================

function [W t] = Ond_Morlet_Munich(li, ls, N, f0, Fs_real)

%t = linspace(li, ls, N);                                                    % Genero espacio lineal entre los l�mites del soporte (eje temporal)
%Ts = (ls - li) / (N - 1);
% Fs = 1/Ts;
k = 0: (N - 1);

Ts = (ls - li) / (N - 1);
Fs = 1/Ts;

t = k * Ts;
t = t + li;

kf0 = 1000; kt = 1;
%kf0 = 1; kt = 1000;

BM = (25 + 75*((1 + 1.4*(f0 / kf0)^ 2)^(0.69)));                            % Est� calculado para f0 en kHz
k0 = ((pi / 1.4)^0.5) * BM;

exp_n1 = exp(-((BM * pi * (t / kt)).^2)/1.4);                               % Est� calculado para el BM en las unidades que le queden...

t_real = k / Fs_real;                                                       % Eje de tiempo para calcular la oscilacion
t_real = t_real - (max(t_real) / 2);                                        % centro el eje de tiempo al cero
exp_n2 = sin(2*pi*f0*t_real);                                               % Oscilacion


W = real( k0 * (exp_n1 .* exp_n2));

end

