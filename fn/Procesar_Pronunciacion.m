%% ========================================================================
%
%>  @file Procesar_Pronunciacion.m
%>  @function [y] = Procesar_Pronunciacion 
%>  @brief version propia del algoritmo de CWT, haciendo la convolucion de 
%>  @brief la senal con la integral de la wavelet desplazada y escalada y 
%>  @brief luego diferenciando esta seal (con el m�todo contrario usado 
%>  @brief para integrar).
%
%>  @author  ROUX, Federico G. (rouxfederico@gmail.com)
%
%>  @company    Profesor: Ing. BRUNO, Julian S.
%>  @company    Procesamiento Digital de Se�ales(PDS)
%>  @company    Departamento de Ing.Electronica.
%>  @company    Facultad Buenos Aires
%>  @company    Universidad Tecnologica Nacional
%% ========================================================================

% El an�lisis se hace en buffer de tama�o de 4ms, como est� en el paper, y
% se solapan algunas pocas muestras (15 de las 64 que tiene el buffer) para
% procesar de una manera m�s cont�nua. 

function libro_procesado = Procesar_Pronunciacion (snd, archivo_wavelet)

Fs_sonido = snd.Fs;                                                         % Frecuencia de muestreo del sonido a procesar

cant_fonemas = length(snd.s);                                               % Cantidad de fonemas contenidos

sonido = 0;                                                                 % Inicializo array de sonidos
separadores_snd = zeros(cant_fonemas, 1);                                   % Inicializo array de separadores

for i_fonema = 1 : cant_fonemas                                             % Recorro todos los fonemas de la estructura
   sonido = [sonido ; snd.s{i_fonema}];                                     % Guardo los sonidos todos continuados
   separadores_snd (i_fonema) = length(snd.s{i_fonema});                    % Guardo el separador asociado para indicar donde termina
end


media_sonido = mean(sonido);
sonido = sonido - media_sonido;                                             % media cero

max_sonido = max(sonido);
min_sonido = min(sonido);
max_abs_sonido = max(abs(max_sonido), abs(min_sonido));
sonido = sonido / max_abs_sonido;                                           % Rango entre +1 y -1


separadores_snd = cumsum(separadores_snd);                                  % pongo las referencias de inicio del fono sobre el final del anterior

L = length(sonido);                                                         % Cantidad total de muestras de la frase

[z1, z2, N, z3, z4, cant_escalas, a, w] = Abrir_Wavelet_Archivo (archivo_wavelet);

ventana_dsp = 4e-3;                                                         % Ventana de procesamiento : 4ms
N_dsp = fix(ventana_dsp * Fs_sonido);                                       % cantidad de muestras a analizar por vez

ESCALAS_ELEGIDAS = 6;                                                       % Cantidad de escalas con mayor RMS elegidas por umbral 
N_SUP_PAPER = 10;                                                           % cantidad de muestras superpuestas en el paper
FS_PAPER = 11025;                                                           % Fs del paper como referencia
N_sup = fix(N_SUP_PAPER *(Fs_sonido / FS_PAPER));                           % Cantidad de muestras superpuestas reales

if(ESCALAS_ELEGIDAS >= cant_escalas)                                        % 
    error('Error en la cantidad de escalas elegidas');
end

if((N_dsp > N)||(N_sup >= N))
    error('Error en la cantidad de muestras de superposici�n o de procesamiento');
end

N_avance = N_dsp - N_sup;                                                   % cantidad de muestras que avanzo 

cant_pag = ceil((L - N_avance) / N_avance);                                 % Cantidad de iteraciones (p�g. de procesamiento)

y = zeros(cant_pag, ESCALAS_ELEGIDAS, N_dsp);                               % Inicializo array de salida

etiquetas = zeros(cant_pag, 1);                                             % Inicializo cantidad de etiquetas

fono_n = 1;                                                                 % Inicializo nro. de fono seleccionado
n_ini = 1;                                                                  % Inicializo nro. de muestra de origen de la ventana

for pag = 1 : cant_pag                                                      % Recorro todas las p�ginas que voy a generar
    
    if pag == cant_pag                                                      % Pregunto si llegu� a la �ltima p�g.
        x = sonido(n_ini:L);                                                % porcion de sonido a procesar al final del array
        if (n_ini - L + 1) < N_dsp                                          % Pregunto si la cant. de muestras que quedan es menor a la de la ventana completa
            y = y(1 : (cant_pag - 1), :, :);                                % Descarto la �ltima p�g
            etiquetas = etiquetas (1 : (cant_pag - 1));                     % Descarto �ltima etiqueta
            break;                                                          % En este caso, descarto el bloque
        end
    else
        x = sonido(n_ini:(n_ini + N_dsp - 1));                              % porcion de sonido a procesar
    end

    if (n_ini > separadores_snd (fono_n))                                   % Si la ventana empieza despu�s del origen del fono
        fono_n = fono_n + 1;                                                % Es porque me pas�, incremento el valor.
        etiquetas(pag) = snd.valores(fono_n);                               % asigno el valor de la etiqueta
    elseif((n_ini + N_dsp - 1) >= separadores_snd(fono_n))                  % si la ventana va m�s all� del fono seleccionado
        cant_muestras_a = separadores_snd(fono_n) - n_ini + 1;              % Porci�n de la ventana del fono a
        cant_muestras_b = N_dsp - cant_muestras_a;                          % Porci�n de la ventana del fono b
        % Guardo el valor ponderado de las etiquetas seg�n la cantidad de
        % muestras de cada porci�n de la ventana
        etiquetas(pag) = ((cant_muestras_a * snd.valores(fono_n)) + (cant_muestras_b * snd.valores(fono_n + 1))) / N_dsp;
    else    
        etiquetas(pag) = snd.valores (fono_n);
    end
    
    
    y_cwt = Calcular_CWT (x, a, w);                                         % proceso la porci�n de se�al
        
    K = size(y_cwt, 1);                                                     % cantidad de muestras de la ondita
    y_rms = sqrt(sum((y_cwt .^2), 2)/ K);                                   % calculo el vector rms
    [y_rms_ord esc_ord]= sort(y_rms);                                       % ordeno para sacar los primeros 6
    
    escalas_seleccionadas = esc_ord (1:ESCALAS_ELEGIDAS);                   % Guardo las primeras 6 escalas
    esc_elegidas_ord = sort(escalas_seleccionadas);                         % Vuelvo a ordenar para respetar los valores de freq.
               
%    y(pag, i_escala, :) = y_cwt(esc_ord (i_escala));                       % Genero p�ginas de procesamiento para entrenar la RNA
    y(pag, :, :) = y_cwt(esc_elegidas_ord, :);                              % Genero p�ginas de procesamiento para entrenar la RNA
   
    
    n_ini = n_ini + N_avance;
end

libro_procesado.libro = y;
libro_procesado.etiq = etiquetas;
libro_procesado.n_dsp = N_dsp;
libro_procesado.n_sup = N_sup;

end