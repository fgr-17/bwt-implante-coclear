%% ========================================================================
%
%>  @file Guardar_Pronunciacion_Procesada.m
%>  @function [y] = Guardar_Pronunciacion_Procesada (libro_proc) 
%>  @brief Recibe un libro de procesamiento con el sonido procesado por 
%>  @brief CWT, del que solo se separaron una serie de escalas (en el paper
%>  @brief son 6), procesados mediante ventanas de 4ms solapando una cierta
%>  @brief cantidad de muestras.
%
%>  @author  ROUX, Federico G. (rouxfederico@gmail.com)
%
%>  @company    Departamento de Ing.Electronica.
%>  @company    Facultad Buenos Aires
%>  @company    Universidad Tecnologica Nacional
%% ========================================================================
function Guardar_Pronunciacion_Procesada (libro_proc, archivo, nombre_fonema)

% La asocaci�n con el archivo original de TIMIT conviene guardarlo en el
% nombre. O mejor agregar un campo con esa informaci�n?
% Guardo un arhcivo con los nombres de archivos separados por criterio de
% clasificaci�n?
fid = fopen(archivo, 'wb');                                                 % abro el archivo que voy a escribir 
fprintf(fid, '/%s/\n',[nombre_fonema 0]);                                         % Guardo nombre de fonema para referencia

cant_etiquetas = length(libro_proc.etiq);                                   % Cantidad de etiquetas
cant_paginas = size(libro_proc.libro, 1);                                   % Cantidad de p�ginas (ventanas de procesamiento)
cant_escalas_elegidas = size(libro_proc.libro, 2);                          % Cantidad de escalas elegidas
cant_muestras_escala = size(libro_proc.libro, 3);                           % Cantidad de muestras por escala

fwrite(fid, cant_etiquetas, 'int16');                                       % Guardo la cantidad de separadores
fwrite(fid, cant_paginas, 'int16');                                         % Cantidad de p�ginas a procesar
fwrite(fid, cant_escalas_elegidas, 'int16');                                % Cantidad de escalas por p�gina
fwrite(fid, cant_muestras_escala, 'int16');                                 % Cantidad de muestras por escala
fwrite(fid, libro_proc.n_dsp, 'int16');                                     % Cantidad de muestras de procesamiento
fwrite(fid, libro_proc.n_sup, 'int16');                                     % Cantidad de muestras superpuestas 

fwrite(fid, libro_proc.etiq, 'double');                                          % Guardo el array de separadores
fwrite(fid, libro_proc.libro, 'double');                                    % Guardo toda la informaci�n procesada

fclose (fid);                                                               % Cierro archivo

end