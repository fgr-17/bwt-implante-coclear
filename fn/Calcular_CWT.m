%%% ========================================================================
%
%>  @file my_cwt.m
%>  @function [y] = my_cwt(x, escalas, wavelet)
%>  @brief version propia del algoritmo de CWT, haciendo la convolucion de 
%>  @brief la senal con la integral de la wavelet desplazada y escalada y 
%>  @brief luego diferenciando esta seal (con el m�todo contrario usado 
%>  @brief para integrar).
%
%>  @author  ROUX, Federico G. (rouxfederico@gmail.com)
%
%>  @company    Profesor: Ing. BRUNO, Julian S.
%>  @company    Procesamiento Digital de Se�ales(PDS)
%>  @company    Departamento de Ing.Electronica.
%>  @company    Facultad Buenos Aires
%>  @company    Universidad Tecnologica Nacional
%% ========================================================================

function [y] = Calcular_CWT(x, escalas, wavelet)

N = length(x);                                                              % Longitud de la se�al
%L = 1024;                                                                   % Longitud inicial de la ondita, sin decimar

escalas_N = length(escalas);                                               % Cantidad de escalas a evaluar

y = zeros(escalas_N, N);                                                    % Inicializo el array con ceros

w0 = wavelet;                                                               % Genero la wavelet completa

% dt = 1 / Fs;                                                                % Calculo la Ts de la Wavelet
% W = cumsum(w0)*dt;                                                          % Calculo la integral numerica por suma acumulativa
% w0T = t(L) - t(1);
%fprintf('\n\n\n\nFUNCION MY_CWT()\n');

for i = 1:escalas_N
    
    a = escalas(i);                                                         % Guardo en 'a' la escala a procesar
%{
     j = 0:(w0T * a);                                                      % En escala 1 tengo 16+1 muestras. Puedo elegir desde 1/16 hasta 64
     j = j / (a * dt);                                                     %                                        
     j = 1 + floor(j);                                                     % Me queda un espacio menos entre la 1er y segunda muestra. Despues est�n equiespaciadas
 %}
%    Wsub = Escalar_Wavelet(W, t, a);                                        % Decimacion de la wavelet   
    % Wsub = W(j);
    w_sub = downsample(w0, a);
    % Winv = fliplr(Wsub);                                                    % Invierto el orden de las muestras para luego hacer la resta       
%    Yt = myConv(Winv, x);                                                   % Hago la convolucion entre la se�al y la integral de la wavelet

    % Yt = wconv1(Winv, x);
    
    yt = myConv(x', w_sub');                                                   % Hago la convoluci�n entre la se�al y la wavelet    
    % yt = diff(Yt);                                                          % Diferencio el array para quitar la integral de la wavelet
        
    y(i, :) = (-1)*sqrt(a) * wkeep1(yt, N);                                     % Me quedo con la parte central    
    %y(i, :) = wkeep1(yt, N);                                                % Me quedo con la parte central
       
end
    
end 