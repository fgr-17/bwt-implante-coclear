%% ========================================================================
%
%>  @file Guardar_Wavelet_Archivo.m
%>  @function Guardar_Wavelet_Archivo (encabezado, a, w)
%>  @brief Guarda una wavelet con el encabezado con informaci�n de su
%>  @brief s�ntesis, las escalas que se eligen para un cierto prop�sito
%>  @brief (por ejemplo, las bandas ERB a analizar) y luego las muestras de
%>  @brief la wavelet sintetizada
%
%>  @author  ROUX, Federico G. (rouxfederico@gmail.com)
%
%>  @company    Profesor: Ing. BRUNO, Julian S.
%>  @company    Procesamiento Digital de Se�ales(PDS)
%>  @company    Departamento de Ing.Electronica.
%>  @company    Facultad Buenos Aires
%>  @company    Universidad Tecnologica Nacional
%% ========================================================================

function Guardar_Wavelet_Archivo (tini, tfin, N, Fs, f0_Hz, cant_escalas, a, w, archivo_dat)

fid2 = fopen (archivo_dat, 'w+b');

% Formato del archivo de la wavelet :
% <<tini>>
% <<tfin>>
% <<N>>
% <<Fs>>
% <<f0_Hz>>
% <<cant_escalas>>
% - escalas en 'float'
% - muestras de la wavelet en 'float'

% Escribo archivo de salida con informaci�n de la wavelet :
fprintf(fid2,   '%f\n%f\n%f\n%f\n%f\n%f\n', ...                             % Formato del encabezado con info. de la wavelet
                tini, ... 
                tfin, ...
                N, ... 
                Fs, ... 
                f0_Hz, ...
                cant_escalas);                                              % Cantidad de escalas que siguen 

fwrite(fid2, a, 'float');                                                   % escribo todo el array de escalas

fwrite(fid2, w, 'double');                                                   % Guardo las muestras de la wavelet 


fclose(fid2);                                                               % Cierro archivo de wavelet

end