%%=========================================================================
%
%>  \funcion Reproducir_Entrada (entrada, ind)
%   
%>  \paramin entrada    cell array con el campo "muestras_audio"
%
%>  \paramin ind        n�mero de entrada
%
%>  \author  Roux, Federico G. (rouxfederico@gmail.com)
%>  \company UTN.BA
%=========================================================================

function Reproducir_Entrada (entrada, ind)

Fs = 16000;                                                                 % Frecuencia de muestreo de la base TIMIT
nBits = 16;                                                                 % cantidad de bits del muestreo


if nargin == 2
    
    cant_entradas_seleccionadas = length(ind);
    
    if cant_entradas_seleccionadas == 1                                     % Reproduzco solo una entrada
        
        soundsc(entrada.muestras_audio{ind}, Fs, nBits);                    % Reproduzco segmento de audio
    
    else                                                                    % Reproduzco una serie de entradas continuas
        audio_completo = null(1);
        for i_ind = 1 : cant_entradas_seleccionadas
            
            audio_completo = vertcat (audio_completo, entrada.muestras_audio{ind(i_ind)});
        end       
        soundsc(audio_completo, Fs, nBits);                            % Reproduzco segmento de audio
        
    end
    
elseif nargin == 1                                                          % Reproduzco todo
    
    cant_fonemas = size(entrada.muestras_audio, 2);                         % Cuento cantidad de fonemas
    audio_completo = null(1);
    
    for i_fonema = 1 : cant_fonemas
    
        audio_completo = vertcat (audio_completo, entrada.muestras_audio{i_fonema});
    end

    soundsc(audio_completo, Fs, nBits);                            % Reproduzco segmento de audio
    
    
end

end