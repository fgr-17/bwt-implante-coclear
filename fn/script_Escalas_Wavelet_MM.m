%% =========================================================================================================================================
%>  @file       Escalas_Wavelet_MM.m
%
%>  @brief      mido la frecuencia central de la wavelet luego de aplicarle
%>  @brief      downsample con valores enteros.
%
%>  @author     Federico Roux (rouxfederico@gmail.com)
%>  @company    RT-DSP UTN.BA
% =========================================================================================================================================

%% Inicializaci�n del Script

clc;
close all;
clear all;
    
%% archivo de salida

archivo_txt = '.\\escalas_elegidas_wavelet_MM.txt';
fid = fopen(archivo_txt, 'w+t');

fprintf(fid, 'Analisis de escalas Wavelet Morlet Munich\n');
fprintf(fid, '------------------------------------------------\n\n');

%% Par�metros para generar la wavelet Morlet

tini = -0.15;                                                               % Inicio del soporte de la wavelet. Sacado de imagen paper BIONIC WAVELET TRANSFORM pag298
tfin = -tini;                                                               % Final del soporte de la wavelet
N = 4096*2;
soporte = tfin - tini;                                                      % soporte de la wavelet Morlet

Ts = soporte / (N - 1);                                                     % Per�odo de muestreo
Fsm = 1 / Ts;
Fs = 16000;                                                                 % Frecuencia de muestreo fija del sistema.
f0_Hz = 50;

% Escribo archivo con informaci�n de escalas de la wavelet elegida :
fprintf(fid, 'Par�metros de s�ntesis: \n');
fprintf(fid, '\t > tini = %.2f \n', tini);
fprintf(fid, '\t > tfin = %.2f \n', tfin);
fprintf(fid, '\t > N = %d \n', N);
fprintf(fid, '\t > Fs = %d [Hz] \n', Fs);
fprintf(fid, '\t > f0 = %d [Hz] \n', f0_Hz);

[w t] = Ond_Morlet_Munich(tini, tfin, N, f0_Hz, Fs);                        % Genero la primer wavelet completa

%% Escalo la wavelet en todas sus posibilidades

fasc = zeros(1, 1024);                                                      % Inicializo el array de frecuencias

a = [1 3 5 7 9 11 14 17 20 23 27 32 36 42 49 57 65 77];
cant_escalas =  length(a) ;                                                 % Guardo la cantidad de escalas

fprintf(fid, '\n\nDetalle de las escalas : \n\n\n');

for i = 1 : length(a)                                                       % Recorro todas las escalas generadas

     w_sub = downsample(w, a(i));
     
     [f0_norm maxI] = Freq_Central_Wavelet(w_sub);                          % Calculo la frecuencia central normalizada de la Wavelet
     [fci_n fcs_n] = Calcular_BW_Wavelet(w_sub);
     fasc(i) = f0_norm*Fs;
     fprintf(fid, 'factor %d: Frecuencia central: %.2f [Hz]\n', a(i),  fasc(i));
     fprintf(fid, 'fci : %.2f [Hz] \t fcs : %.2f [Hz] \t BW : %.2f \n\n ', fci_n* Fs, fcs_n * Fs, (fcs_n - fci_n) * Fs);
     
     %% GRAFICO DE LOS RESULTADOS
     %{
     subplot(2, 1, 1);
     Grafico_Temporal(w_sub, Fsm, tini, 'Grafico Temporal', 't[s]', 'Psi(t)', '-g', 0, 'Wavelet Morlet B Munich');   
     subplot(2, 1, 2);
     W_SUB = Grafico_Frecuencial(w_sub, Fs, '-g', 3, 2, 1, 2);              % Grafico frecuencia y guardo el espectro
     text(fasc(i), W_SUB(maxI), sprintf('fmax = %.2f[Hz]', fasc(i)));       % Muestro etiqueta con la frec. m�xima 
     sKey = input('Continuar','s');
     %}
end

%% Guardo archivo de salida de wavelet para el procesamiento : 
% falta ver como le llamo al archivo de la wavelet :

archivo_dat = 'G:\IIA\archivos de salida\wav1.dat';
Guardar_Wavelet_Archivo (tini, tfin, N, Fs, f0_Hz,  cant_escalas, a, w, archivo_dat);

%% Cierro archivo de texto y salgo :
fclose(fid);                                                                % Cierro archivo de escalas
