%% ========================================================================
%
%>  @function array = Libro_a_Array (libro_est)
%>  @brief  Recibe un libro procesado y lo transforma en array para entrenar
%>  @brief  una red.
%
%>  @author  ROUX, Federico G. (rouxfederico@gmail.com)
%
%>  @company    Departamento de Ing.Electronica.
%>  @company    Facultad Buenos Aires
%>  @company    Universidad Tecnologica Nacional
%% ========================================================================

function array = Libro_a_Array (libro_est)


% array = null(1);                                                            % Inicializo la matriz como vac�a
R_entrada = libro_est.n_dsp * libro_est.n_esc;                              % Array desarrollado : cantidad de neuronas de entrada
array = zeros(R_entrada, libro_est.n_pag);

for i_pag = 1 : libro_est.n_pag

    for i_esc = 1 : libro_est.n_esc
    
        fila_ini = (libro_est.n_dsp * (i_esc - 1)) + 1;
        fila_fin = fila_ini + libro_est.n_dsp - 1;
        array (fila_ini:fila_fin, i_pag) = shiftdim(libro_est.libro(i_pag, i_esc, :));
        
    end
        
end
    
    
end