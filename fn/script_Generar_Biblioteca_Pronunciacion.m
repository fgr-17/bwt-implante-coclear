%% ========================================================================
%
%>  @file script_Generar_Biblioteca_Pronunciacion.m
%
%>  @brief  extraigo los fonemas que necesito de la base de datos de TIMIT
%>  @brief  para despu�s procesarlos con CWT y guardarlos en archivos
%
%>  @author     Roux, Federico G. (rouxfederico@gmail.com)
%>  @company    UTN.BA 2014
%% ========================================================================

%% ==================== Inicializaci�n del script ========================= 

clear all script_Generar_Biblioteca_Pronunciacion.m
close all 
clc;
disp(' ==============================================================');
disp(' -                SEGMENTACION DE FONEMAS                     -');
disp(' ==============================================================');
fprintf(1, '\n\n');
textprogressbar('Extrayendo sonidos de TIMIT           ');

%% ======== Valuaci�n de los fonemas extraidos : CONSONANTES =============

% Se eval�an las consonantes en contextos /vCv/, es decir, vocal,
% consonante vocal :

rna(1).descripcion = 'no';                                                  % El frame no corresponde a voz
rna(1).valor = 0;                                                           % Valuo la salida de la red con un 0
    
rna(2).descripcion = 'V';                                                   % El frame corresponde a vocal en contexto
rna(2).valor = 2;                                                           % Valuo salida de la RNA con un 2
    
rna(3).descripcion = '/tV/';                                                % Valoracion de las salidas de la RNA a consonantes seguidas de vocal
rna(3).valor = 4;
rna(4).descripcion = '/kV/';
rna(4).valor = 5;
rna(5).descripcion = '/dV/';
rna(5).valor = 6;
rna(6).descripcion = '/bV/';
rna(6).valor = 7;
rna(7).descripcion = '/pV/';
rna(7).valor = 8;
rna(8).descripcion = '/gV/';
rna(8).valor = 9;
rna(9).descripcion = '/jV/';
rna(9).valor = 10;

%% ====== Armo el set de sonidos a analizar :  =========
textprogressbar(10);
Fs = 16000;                                                                 % Fs de la base TIMIT

% -------------------------------------------------------------------------
% Extraigo ordenando por nombre de carpeta y archivo :
% -------------------------------------------------------------------------
%% Archivo FAKS0\SA2 : Extraigo un solo sonido
faks0_sa1 = Extraer_Fonemas('\FAKS0\SA1');                                  % Entrada : 0 63488 She had your dark suit in greasy wash water all year.

snd{1}{1}.s{1} = [faks0_sa1.fonemas.muestras_audio{10}; faks0_sa1.fonemas.muestras_audio{11}];                      % extraigo "Dark"
snd{1}{1}.s{2} = faks0_sa1.fonemas.muestras_audio{12};                      % extraigo "dArk"
snd{1}{1}.nombre = 'faks0_sa1';                                             % nombre del archivo extra�do. Guardo para referencia
snd{1}{1}.nom_fonemas = 'da';                                               % Nombre del sonido que busco procesar
snd{1}{1}.valores = [rna(5).valor rna(2).valor];
snd{1}{1}.Fs = Fs;

%% Archivo FAKS0\SA2 : Extraigo 3 sonidos 
faks0_sa2 = Extraer_Fonemas ('\FAKS0\SA2');                                 % Entrada : 0 58061 Don't ask me to carry an oily rag like that.
snd{2}{1}.s{1} = faks0_sa2.fonemas.muestras_audio{2};                       % extraigo "Don't"
snd{2}{1}.s{2} = faks0_sa2.fonemas.muestras_audio{3};                       % extraigo "dOn't"
snd{2}{1}.nombre = 'faks0_sa2';                                             % nombre del archivo extra�do. Guardo para referencia
snd{2}{1}.nom_fonemas = 'do';                                               % Nombre del sonido que busco procesar
snd{2}{1}.valores = [rna(5).valor rna(2).valor];
snd{2}{1}.Fs = Fs;

snd{2}{2}.s{1} = [faks0_sa2.fonemas.muestras_audio{10}; faks0_sa2.fonemas.muestras_audio{11}];   % extraigo "To"
snd{2}{2}.s{2} = faks0_sa2.fonemas.muestras_audio{12};                      % extraigo "tO"             
snd{2}{2}.nombre = 'faks0_sa2';                                             % nombre del archivo extra�do. Guardo para referencia
snd{2}{2}.nom_fonemas = 'to';
snd{2}{2}.valores = [rna(3).valor rna(2).valor];
snd{2}{2}.Fs = Fs;

snd{2}{2}.s{1} = [faks0_sa2.fonemas.muestras_audio{13}; faks0_sa2.fonemas.muestras_audio{14}];   % extraigo "To"
snd{2}{2}.s{2} = faks0_sa2.fonemas.muestras_audio{15};                      % extraigo "tO"             
snd{2}{2}.nombre = 'faks0_sa2';                                             % nombre del archivo extra�do. Guardo para referencia
snd{2}{2}.nom_fonemas = 'ka';
snd{2}{2}.valores = [rna(4).valor rna(2).valor];
snd{2}{2}.Fs = Fs;

%% Archivo '.\FAKS0\SI943.TXT' : Extraigo 4 sonidos 
%{
faks0_si943 = Extraer_Fonemas ('\FAKS0\SI943');                                
snd{3}{1}.s{1} = [faks0_si943.fonemas.muestras_audio{4} faks0_si943.fonemas.muestras_audio{5}];
snd{3}{1}.s{2} = faks0_si943.fonemas.muestras_audio{6};                       
snd{3}{1}.nombre = 'faks0_si943';                                             
snd{3}{1}.nom_fonemas = 'de';                                               
snd{3}{1}.valores = [rna(5).valor rna(2).valor];
snd{3}{1}.Fs = Fs;

snd{3}{2}.s{1} = faks0_si943.fonemas.muestras_audio{21};
snd{3}{2}.s{2} = faks0_si943.fonemas.muestras_audio{22};                       
snd{3}{2}.nombre = 'faks0_si943';                                             
snd{3}{2}.nom_fonemas = 'be';                                               
snd{3}{2}.valores = [rna(6).valor rna(2).valor];
snd{3}{2}.Fs = Fs;

snd{3}{3}.s{1} = [faks0_si943.fonemas.muestras_audio{29} faks0_si943.fonemas.muestras_audio{30}];
snd{3}{3}.s{2} = faks0_si943.fonemas.muestras_audio{31};   
snd{3}{3}.nombre = 'faks0_si943';                                             
snd{3}{3}.nom_fonemas = 'pe';                                               
snd{3}{3}.valores = [rna(7).valor rna(2).valor];
snd{3}{3}.Fs = Fs;

snd{3}{4}.s{1} = faks0_si943.fonemas.muestras_audio{21};
snd{3}{4}.s{2} = faks0_si943.fonemas.muestras_audio{22};                       
snd{3}{4}.nombre = 'faks0_si943';                                             
snd{3}{4}.nom_fonemas = 'te';                                               
snd{3}{4}.valores = [rna(6).valor rna(2).valor];
snd{3}{4}.Fs = Fs;
%}

%% Archivo '.\FAKS0\SI1573.TXT' : Extraigo 4 sonidos 
textprogressbar(10);
faks0_si1573 = Extraer_Fonemas ('\FAKS0\SI1573');                                

snd{3}{1}.s{1} = [faks0_si1573.fonemas.muestras_audio{5}; faks0_si1573.fonemas.muestras_audio{6}];
snd{3}{1}.s{2} = faks0_si1573.fonemas.muestras_audio{7};                       
snd{3}{1}.nombre = 'faks0_si1573';                                             
snd{3}{1}.nom_fonemas = 'ka';                                               
snd{3}{1}.valores = [rna(4).valor rna(2).valor];
snd{3}{1}.Fs = Fs;

snd{3}{2}.s{1} = faks0_si1573.fonemas.muestras_audio{9};
snd{3}{2}.s{2} = faks0_si1573.fonemas.muestras_audio{10};   
snd{3}{2}.nombre = 'faks0_si1573';                                             
snd{3}{2}.nom_fonemas = 'te';                                               
snd{3}{2}.valores = [rna(3).valor rna(2).valor];
snd{3}{2}.Fs = Fs;

snd{3}{3}.s{1} = [faks0_si1573.fonemas.muestras_audio{25}; faks0_si1573.fonemas.muestras_audio{26}];
snd{3}{3}.s{2} = faks0_si1573.fonemas.muestras_audio{27};                       
snd{3}{3}.nombre = 'faks0_si1573';                                             
snd{3}{3}.nom_fonemas = 'dae';                                               
snd{3}{3}.valores = [rna(5).valor rna(2).valor];
snd{3}{3}.Fs = Fs;

snd{3}{4}.s{1} = [faks0_si1573.fonemas.muestras_audio{31}; faks0_si1573.fonemas.muestras_audio{32}];
snd{3}{4}.s{2} = faks0_si1573.fonemas.muestras_audio{33};                       
snd{3}{4}.nombre = 'faks0_si1573';                                             
snd{3}{4}.nom_fonemas = 'bju';                                               
snd{3}{4}.valores = [rna(6).valor rna(2).valor];
snd{3}{4}.Fs = Fs;

snd{3}{5}.s{1} = faks0_si1573.fonemas.muestras_audio{35};
snd{3}{5}.s{2} = faks0_si1573.fonemas.muestras_audio{36};                       
snd{3}{5}.nombre = 'faks0_si1573';                                             
snd{3}{5}.nom_fonemas = 'du';                                               
snd{3}{5}.valores = [rna(5).valor rna(2).valor];
snd{3}{5}.Fs = Fs;

snd{3}{6}.s{1} = [faks0_si1573.fonemas.muestras_audio{39}; faks0_si1573.fonemas.muestras_audio{40}];
snd{3}{6}.s{2} = faks0_si1573.fonemas.muestras_audio{40}; 
snd{3}{6}.nombre = 'faks0_si1573';                                             
snd{3}{6}.nom_fonemas = 'bu';                                               
snd{3}{6}.valores = [rna(6).valor rna(2).valor];
snd{3}{6}.Fs = Fs;

%% Archivo '.\FAKS0\SI2203.TXT' : Extraigo 4 sonidos 

faks0_si2203 = Extraer_Fonemas ('\FAKS0\SI2203');                                

snd{4}{1}.s{1} = [faks0_si2203.fonemas.muestras_audio{15}; faks0_si2203.fonemas.muestras_audio{16}];
snd{4}{1}.s{2} = faks0_si2203.fonemas.muestras_audio{17};                       
snd{4}{1}.nombre = 'faks0_si2203';                                             
snd{4}{1}.nom_fonemas = 'da';                                               
snd{4}{1}.valores = [rna(5).valor rna(2).valor];
snd{4}{1}.Fs = Fs;
textprogressbar(20);
%% Archivo 'FAKS0\SX43'
faks0_sx43 = Extraer_Fonemas('\FAKS0\SX43');                                % Entrada : 0 39220 Elderly people are often excluded.

snd{5}{1}.s{1} = [faks0_sx43.fonemas.muestras_audio{5}; faks0_sx43.fonemas.muestras_audio{6}];
snd{5}{1}.s{2} = faks0_sx43.fonemas.muestras_audio{7};                       
snd{5}{1}.nombre = 'faks0_sx43';                                             
snd{5}{1}.nom_fonemas = 'de';                                               
snd{5}{1}.valores = [rna(5).valor rna(2).valor];
snd{5}{1}.Fs = Fs;

snd{5}{2}.s{1} = [faks0_sx43.fonemas.muestras_audio{10}; faks0_sx43.fonemas.muestras_audio{11}];
snd{5}{2}.s{2} = faks0_sx43.fonemas.muestras_audio{12};
snd{5}{2}.nombre = 'faks0_sx43';                                             
snd{5}{2}.nom_fonemas = 'pi';                                               
snd{5}{2}.valores = [rna(7).valor rna(2).valor];
snd{5}{2}.Fs = Fs;

snd{5}{3}.s{1} = [faks0_sx43.fonemas.muestras_audio{13}; faks0_sx43.fonemas.muestras_audio{14}];
snd{5}{3}.s{2} = faks0_sx43.fonemas.muestras_audio{15};                       
snd{5}{3}.nombre = 'faks0_sx43';                                             
snd{5}{3}.nom_fonemas = 'pe';                                               
snd{5}{3}.valores = [rna(7).valor rna(2).valor];
snd{5}{3}.Fs = Fs;

snd{5}{4}.s{1} = [faks0_sx43.fonemas.muestras_audio{30}; faks0_sx43.fonemas.muestras_audio{31}];
snd{5}{4}.s{2} = faks0_sx43.fonemas.muestras_audio{32};
snd{5}{4}.nombre = 'faks0_sx43';                                             
snd{5}{4}.nom_fonemas = 'de';                                               
snd{5}{4}.valores = [rna(5).valor rna(2).valor];
snd{5}{4}.Fs = Fs;

%% Archivo FAKS0\SX133

faks0_sx133 = Extraer_Fonemas('\FAKS0\SX133');                                % Entrada : 0 39220 Elderly people are often excluded.

snd{6}{1}.s{1} = faks0_sx43.fonemas.muestras_audio{2};
snd{6}{1}.s{2} = faks0_sx43.fonemas.muestras_audio{3};                       
snd{6}{1}.nombre = 'faks0_sx133';                                             
snd{6}{1}.nom_fonemas = 'pi';                                               
snd{6}{1}.valores = [rna(7).valor rna(2).valor];
snd{6}{1}.Fs = Fs;

snd{6}{2}.s{1} = [faks0_sx43.fonemas.muestras_audio{13}; faks0_sx43.fonemas.muestras_audio{14}];
snd{6}{2}.s{2} = faks0_sx43.fonemas.muestras_audio{15};                       
snd{6}{2}.nombre = 'faks0_sx133';                                             
snd{6}{2}.nom_fonemas = 'ke';                                               
snd{6}{2}.valores = [rna(4).valor rna(2).valor];
snd{6}{2}.Fs = Fs;

%% Archivo FAKS0\SX223
textprogressbar(30);
faks0_sx223 = Extraer_Fonemas('\FAKS0\SX223');                                % Entrada : 0 39220 Elderly people are often excluded.

snd{7}{1}.s{1} = faks0_sx223.fonemas.muestras_audio{2};
snd{7}{1}.s{2} = faks0_sx223.fonemas.muestras_audio{3};                       
snd{7}{1}.nombre = 'faks0_sx223';                                             
snd{7}{1}.nom_fonemas = 'pu';                                               
snd{7}{1}.valores = [rna(7).valor rna(2).valor];
snd{7}{1}.Fs = Fs;

snd{7}{2}.s{1} = [faks0_sx223.fonemas.muestras_audio{7}; faks0_sx223.fonemas.muestras_audio{8}];
snd{7}{2}.s{2} = faks0_sx223.fonemas.muestras_audio{9};                       
snd{7}{2}.nombre = 'faks0_sx223';                                             
snd{7}{2}.nom_fonemas = 'bu';                                               
snd{7}{2}.valores = [rna(6).valor rna(2).valor];
snd{7}{2}.Fs = Fs;

snd{7}{3}.s{1} = faks0_sx223.fonemas.muestras_audio{18};
snd{7}{3}.s{2} = faks0_sx223.fonemas.muestras_audio{19};                       
snd{7}{3}.nombre = 'faks0_sx233';                                             
snd{7}{3}.nom_fonemas = 'ta';                                               
snd{7}{3}.valores = [rna(3).valor rna(2).valor];
snd{7}{3}.Fs = Fs;

snd{7}{4}.s{1} = [faks0_sx223.fonemas.muestras_audio{27}; faks0_sx223.fonemas.muestras_audio{28}];
snd{7}{4}.s{2} = faks0_sx223.fonemas.muestras_audio{29};                       
snd{7}{4}.nombre = 'faks0_sx223';                                             
snd{7}{4}.nom_fonemas = 'ge';                                               
snd{7}{4}.valores = [rna(8).valor rna(2).valor];
snd{7}{4}.Fs = Fs;

%% Archivo FAKS0\SX313

faks0_sx313 = Extraer_Fonemas('\FAKS0\SX313');                                % Entrada : 0 39220 Elderly people are often excluded.

snd{8}{1}.s{1} = [faks0_sx313.fonemas.muestras_audio{18}; faks0_sx313.fonemas.muestras_audio{19}];
snd{8}{1}.s{2} = faks0_sx313.fonemas.muestras_audio{20};
snd{8}{1}.nombre = 'faks0_sx313';                                             
snd{8}{1}.nom_fonemas = 'ba';                                               
snd{8}{1}.valores = [rna(6).valor rna(2).valor];
snd{8}{1}.Fs = Fs;

snd{8}{2}.s{1} = [faks0_sx313.fonemas.muestras_audio{24}; faks0_sx313.fonemas.muestras_audio{25}];
snd{8}{2}.s{2} = faks0_sx313.fonemas.muestras_audio{26};                       
snd{8}{2}.nombre = 'faks0_sx313';                                             
snd{8}{2}.nom_fonemas = 'be';                                               
snd{8}{2}.valores = [rna(6).valor rna(2).valor];
snd{8}{2}.Fs = Fs;

snd{8}{3}.s{1} = [faks0_sx313.fonemas.muestras_audio{32}; faks0_sx313.fonemas.muestras_audio{33}];
snd{8}{3}.s{2} = faks0_sx313.fonemas.muestras_audio{34};                       
snd{8}{3}.nombre = 'faks0_sx313';                                             
snd{8}{3}.nom_fonemas = 'go';                                               
snd{8}{3}.valores = [rna(8).valor rna(2).valor];
snd{8}{3}.Fs = Fs;

%% Archivo FAKS0\SX403
textprogressbar(40);
faks0_sx403 = Extraer_Fonemas('\FAKS0\SX403');                                % Entrada : 0 39220 Elderly people are often excluded.

snd{9}{1}.s{1} = faks0_sx403.fonemas.muestras_audio{12};
snd{9}{1}.s{2} = faks0_sx403.fonemas.muestras_audio{13};                       
snd{9}{1}.nombre = 'faks0_sx403';                                             
snd{9}{1}.nom_fonemas = 'ke';                                               
snd{9}{1}.valores = [rna(7).valor rna(2).valor];
snd{9}{1}.Fs = Fs;

snd{9}{2}.s{1} = [faks0_sx403.fonemas.muestras_audio{24}; faks0_sx403.fonemas.muestras_audio{25}];
snd{9}{2}.s{2} = faks0_sx403.fonemas.muestras_audio{26};
snd{9}{2}.nombre = 'faks0_sx403';                                             
snd{9}{2}.nom_fonemas = 'ke';                                               
snd{9}{2}.valores = [rna(6).valor rna(2).valor];
snd{9}{2}.Fs = Fs;

%% Archivo FDAC\SA1.TXT

fdac1_sa1 = Extraer_Fonemas('\FDAC1\SA1');                                  % Entrada : 0 84480 She had your dark suit in greasy wash water all year.
snd{10}{1}.s{1} = fdac1_sa1.fonemas.muestras_audio{11};                      % extraigo "Dark"
snd{10}{1}.s{2} = fdac1_sa1.fonemas.muestras_audio{12};                      % extraigo "dArk"
snd{10}{1}.nombre = 'fdac1_sa1';                                             % nombre del archivo extra�do. Guardo para referencia
snd{10}{1}.nom_fonemas = 'da';                                               % Nombre del sonido que busco procesar
snd{10}{1}.valores = [rna(5).valor rna(2).valor];
snd{10}{1}.Fs = Fs;
% -------------------------------------------------------------------------
% /de/ : 
% -------------------------------------------------------------------------
textprogressbar(60);

% -------------------------------------------------------------------------
% /di/ :
% -------------------------------------------------------------------------

snd{11}{1}.s{1} = faks0_sx43.fonemas.muestras_audio{31};                     % extraigo "excluDed"
snd{11}{1}.s{2} = faks0_sx43.fonemas.muestras_audio{32};                     % extraigo "excludEd"
snd{11}{1}.nombre = 'faks0_sx43';                                            % nombre del archivo extra�do. Guardo para referencia
snd{11}{1}.nom_fonemas = 'di';                                               % Nombre del sonido que busco procesar
snd{11}{1}.valores = [rna(5).valor rna(2).valor];
snd{11}{1}.Fs = Fs;
% -------------------------------------------------------------------------
% /do/ :
% -------------------------------------------------------------------------


% -------------------------------------------------------------------------
% /du/
% -------------------------------------------------------------------------

fdac1_sx34 = Extraer_Fonemas ('\FDAC1\SX34');                               % Entrada : 
snd{12}{1}.s{1} = fdac1_sx34.fonemas.muestras_audio{6};                      % extraigo "Du"
snd{12}{1}.s{2} = fdac1_sx34.fonemas.muestras_audio{7};                      % extraigo "du"
snd{12}{1}.nombre = 'fdac1_sx34';                                            % nombre del archivo extra�do. Guardo para referencia
snd{12}{1}.nom_fonemas = 'du';                                               % Nombre del sonido que busco procesar
snd{12}{1}.valores = [rna(5).valor rna(2).valor];
snd{12}{1}.Fs = Fs;
textprogressbar(100);
textprogressbar('Finalizado');
%% ================== Proceso los sonidos a analizar : ====================

% En esta secci�n los sonidos a analizar son de la forma /cv/, es decir,
% consonante seguido por una vocal en una pronunciaci�n flu�da. Una vez
% extra�dos los sonidos de la base de datos, se guardan en forma separada,
% pero para analizarlos se unen los 2 sonidos en un solo array. 
   
    textprogressbar('Generando libros procesados           ');

    
path_propio = mfilename('fullpath');                                        % Obtengo el path propio sin extension
path_desde_base = 'fn\script_Generar_Biblioteca_Pronunciacion';             % Path fijo del archivo en la base
path_base = strrep(path_propio, path_desde_base, '');                       % Extraigo path de la base
    
    
path_wav = [path_base 'archivos de salida\'];                                        % Ruta donde guardo archivos de salida
archivo_wav = [path_wav 'wav1.dat'];
cant_snd = length(snd);                                                     % guardo la cantidad de sonidos a analizar


path_lib_sin_base = 'archivos de salida\CWT_SNR_02dB\';
path_bib = [path_base path_lib_sin_base];
mkdir(path_bib);
archivo_bib = [path_bib 'FAKS0.bib'];                                         % lista de archivos con la pronunciacion dV
fid_bib = fopen (archivo_bib, 'wt');                                        % Abro para guardar texto (nombres de archivos)



for i = 1:cant_snd                                                          % Analizo todos los sonidos
    
    cant_ent = length(snd{i});                                              % cuento cant. de sonidos de este tipo 
    
    for j = 1 : cant_ent                
       
        textprogressbar((i* 100)/cant_snd)

        lib_proc = Procesar_Pronunciacion_CWT_RUIDO(snd{i}{j}, archivo_wav, 0.2); % Proceso el sonido elegido

        archivo_lib = [path_bib snd{i}{j}.nombre '_' snd{i}{j}.nom_fonemas '.lbr'];                       % Archivo donde guardo el libro a procesar
        archivo_lib_sin_base = strrep(archivo_lib, path_base, '');
  
        fprintf(fid_bib, '%s\n', archivo_lib_sin_base);
        
        Guardar_Pronunciacion_Procesada (lib_proc, archivo_lib, snd{i}{j}.nom_fonemas );            % GUARDO libro de procesamineto para entrenar la red        

    end 
    
end
    textprogressbar('Finalizado');

fclose (fid_bib);                                                           % Cierro biblioteca de sonidos
fclose all;
fprintf(1, '\n\nSe finaliz� la escritura de libros de procesamiento\n\n');

