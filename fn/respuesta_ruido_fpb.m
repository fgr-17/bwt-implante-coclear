
%% ========================================================================
%
%>  @file   respuesta_ruido.m
%
%>  @author Roux, Federico G. (rouxfederico@gmail.com)
%>  @company    UTN.BA 2014
%% ========================================================================

%% ==================== Inicialización del script ========================= 


clear all;
fclose all;
close all 
clc;
disp(' ==============================================================');
disp(' -              PREPARACION DEL SET DE DATOS                  -');
disp(' ==============================================================');
fprintf(1, '\n\n');
%% Abro archivo de biblioteca :7

path_propio = mfilename('fullpath');                                        % Obtengo el path propio sin extension
path_desde_base = 'fn\respuesta_ruido';                               % Path fijo del archivo en la base
path_base = strrep(path_propio, path_desde_base, '');                       % Extraigo path de la base


path_bib = [path_base 'archivos de salida\FPB_SNR_02dB\'];                              % Ruta donde guardo archivos de salida
archivo_bib = [path_bib 'FAKS0.bib'];                                       % lista de archivos con la pronunciacion dV
fid_bib = fopen (archivo_bib, 'rt');                                        % Abro para guardar texto (nombres de archivos)

i_libro = 1;

entrada = null(1);
target = null(1);

textprogressbar('Abriendo libros procesados           ');
%% Extraigo archivos de la biblioteca :


while(not(feof(fid_bib)))
    
    textprogressbar(10);
    archivo_lbr = fgets(fid_bib);                                                           % Leo una linea : corresponde a un archivo de libro
    archivo_lbr = [path_base archivo_lbr(1:(length(archivo_lbr) - 1))];                                 % Le quito el caracter de ENTER
    [libro_proc{i_libro}, nombre_fonema] = Abrir_Pronunciacion_Procesada (archivo_lbr);     % Abro el archivo y extraigo la información
    
    textprogressbar(40);
    array_libro = Libro_a_Array (libro_proc{i_libro});
    
    entrada = horzcat(entrada, array_libro);
    target = horzcat(target, libro_proc{i_libro}.etiq');
    i_libro = i_libro + 1;                                                                  % paso al siguiente libro
    textprogressbar(100);
end
fclose(fid_bib);
textprogressbar('Finalizado');

%% EVALUO RED FPB :

open net_fpb_75.mat
net_fpb = ans.net;

open tr_fpb_75.mat
tr_fpb = ans.tr;


output_fpb = net_fpb(entrada);                                                      % Simulo la salida para el entrenamiento hecho
output_train_fpb = output_fpb(tr_fpb.trainInd);                                         % Salida del set de entrenamiento
output_val_fpb = output_fpb(tr_fpb.valInd);                                             % Salida del set de validacion
output_test_fpb = output_fpb(tr_fpb.testInd);                                           % Salida del set de testeo

target_train_fpb = target(tr_fpb.trainInd);                                         % Targets del set de entrenamiento
target_val_fpb = target(tr_fpb.valInd);                                             % Targets del set de validacion
target_test_fpb = target(tr_fpb.testInd);                                           % Targets del set de testeo

regresion_train_fpb = regression(target_train_fpb, output_train_fpb);
regresion_val_fpb = regression(target_val_fpb, output_val_fpb);
regresion_test_fpb = regression(target_test_fpb, output_test_fpb);
regresion_total_fpb = regression(target, output_fpb);

fprintf(1, '\t> Respuesta de la red fpb para SNR = 0.2 dB :\n');
fprintf(1, '\t\t - Regresion Entrenamiento : %f\n', regresion_train_fpb);
fprintf(1, '\t\t - Regresion Validacion : %f\n', regresion_val_fpb);
fprintf(1, '\t\t - Regresion Test : %f\n', regresion_test_fpb);
fprintf(1, '\t\t - Regresion Total : %f\n', regresion_total_fpb);