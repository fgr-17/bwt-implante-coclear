%% ========================================================================
%
%>  @file script_Generar_Libros_Procesados.m
%
%>  @brief  extraigo los fonemas que necesito de la base de datos de TIMIT
%>  @brief  para despu�s procesarlos con CWT y guardarlos en archivos
%
%>  @author Roux, Federico G. (rouxfederico@gmail.com)
%>  @company    UTN.BA 2014
%% ========================================================================

%% ========================================================================
% ---- RESUMEN DEL TRABAJO : ----
%
% 1) Extraer fonemas de una frase
%       agregar extraer encabezado
% 2) Recortar los fonemas en ventanas de 4ms. Revisar Fs y ventana del
% paper
% 3) Hacer la cwt propia usando la w-mm
% 4) Calcular RMS y separar las 6 bandas con mayor energia
% 5) Rearmar la se�al de audio?
% 6) Emperzar a trabajar en la ANN
%% ========================================================================

%% ==================== Inicializaci�n del script ========================= 

clear all script_trabajo.m
close all 
clc;
disp(' ==============================================================');
disp(' -                SEGMENTACION DE FONEMAS                     -');
disp(' ==============================================================');
fprintf(1, '\n\n');
textprogressbar('Extrayendo sonidos de TIMIT           ');

%% ======== Valuaci�n de los fonemas extraidos : CONSONANTES =============

% Se eval�an las consonantes en contextos /vCv/, es decir, vocal,
% consonante vocal :

rna(1).descripcion = 'no';                                                  % El frame no corresponde a voz
rna(1).valor = 0;                                                           % Valuo la salida de la red con un 0
    
rna(2).descripcion = 'V';                                                   % El frame corresponde a vocal en contexto
rna(2).valor = 2;                                                           % Valuo salida de la RNA con un 2
    
rna(3).descripcion = '/tV/';                                                % Valoracion de las salidas de la RNA a consonantes seguidas de vocal
rna(3).valor = 4;
rna(4).descripcion = '/kV/';
rna(4).valor = 5;
rna(5).descripcion = '/dV/';
rna(5).valor = 6;
rna(6).descripcion = '/bV/';
rna(6).valor = 7;
rna(7).descripcion = '/pV/';
rna(7).valor = 8;
rna(8).descripcion = '/gV/';
rna(8).valor = 9;
rna(9).descripcion = '/jV/';
rna(9).valor = 10;

%% ====== Armo el set de sonidos a analizar : reconocimiento /dV/ =========
textprogressbar(10);
Fs = 16000;                                                                 % Fs de la base TIMIT

% -------------------------------------------------------------------------
% /da/ :
% -------------------------------------------------------------------------

faks0_sa1 = Extraer_Fonemas('\FAKS0\SA1');                                  % Entrada : 0 63488 She had your dark suit in greasy wash water all year.
snd{1}{1}.s{1} = faks0_sa1.fonemas.muestras_audio{11};                      % extraigo "Dark"
snd{1}{1}.s{2} = faks0_sa1.fonemas.muestras_audio{12};                      % extraigo "dArk"
snd{1}{1}.nombre = 'faks0_sa1';                                             % nombre del archivo extra�do. Guardo para referencia
snd{1}{1}.nom_fonemas = 'da';                                               % Nombre del sonido que busco procesar
snd{1}{1}.valores = [rna(2).valor rna(5).valor];
snd{1}{1}.Fs = Fs;

fdac1_sa1 = Extraer_Fonemas('\FDAC1\SA1');                                  % Entrada : 0 84480 She had your dark suit in greasy wash water all year.
snd{1}{2}.s{1} = fdac1_sa1.fonemas.muestras_audio{11};                      % extraigo "Dark"
snd{1}{2}.s{2} = fdac1_sa1.fonemas.muestras_audio{12};                      % extraigo "dArk"
snd{1}{2}.nombre = 'fdac1_sa1';                                             % nombre del archivo extra�do. Guardo para referencia
snd{1}{2}.nom_fonemas = 'da';                                               % Nombre del sonido que busco procesar
snd{1}{2}.valores = [rna(2).valor rna(5).valor];
snd{1}{2}.Fs = Fs;
% -------------------------------------------------------------------------
% /de/ : 
% -------------------------------------------------------------------------
textprogressbar(20);
faks0_sx43 = Extraer_Fonemas('\FAKS0\SX43');                                % Entrada : 0 39220 Elderly people are often excluded.
snd{2}{1}.s{1} = faks0_sx43.fonemas.muestras_audio{6};                      % extraigo "elDerly"
snd{2}{1}.s{2} = faks0_sx43.fonemas.muestras_audio{7};                      % extraigo "eldErly"
snd{2}{1}.nombre = 'faks0_sx43';                                            % nombre del archivo extra�do. Guardo para referencia
snd{2}{1}.nom_fonemas = 'de';                                               % Nombre del sonido que busco procesar
snd{2}{1}.valores = [rna(2).valor rna(5).valor];
snd{2}{1}.Fs = Fs;
% -------------------------------------------------------------------------
% /di/ :
% -------------------------------------------------------------------------

snd{3}{1}.s{1} = faks0_sx43.fonemas.muestras_audio{31};                     % extraigo "excluDed"
snd{3}{1}.s{2} = faks0_sx43.fonemas.muestras_audio{32};                     % extraigo "excludEd"
snd{3}{1}.nombre = 'faks0_sx43';                                            % nombre del archivo extra�do. Guardo para referencia
snd{3}{1}.nom_fonemas = 'di';                                               % Nombre del sonido que busco procesar
snd{3}{1}.valores = [rna(2).valor rna(5).valor];
snd{3}{1}.Fs = Fs;
% -------------------------------------------------------------------------
% /do/ :
% -------------------------------------------------------------------------
textprogressbar(60);
faks0_sa2 = Extraer_Fonemas ('\FAKS0\SA2');                                 % Entrada : 0 58061 Don't ask me to carry an oily rag like that.
snd{4}{1}.s{1} = faks0_sa2.fonemas.muestras_audio{2};                       % extraigo "Don't"
snd{4}{1}.s{2} = faks0_sa2.fonemas.muestras_audio{3};                       % extraigo "dOn't"
snd{4}{1}.nombre = 'faks0_sa2';                                             % nombre del archivo extra�do. Guardo para referencia
snd{4}{1}.nom_fonemas = 'do';                                               % Nombre del sonido que busco procesar
snd{4}{1}.valores = [rna(2).valor rna(5).valor];
snd{4}{1}.Fs = Fs;
% -------------------------------------------------------------------------
% /du/
% -------------------------------------------------------------------------

fdac1_sx34 = Extraer_Fonemas ('\FDAC1\SX34');                               % Entrada : 
snd{5}{1}.s{1} = fdac1_sx34.fonemas.muestras_audio{6};                      % extraigo "Du"
snd{5}{1}.s{2} = fdac1_sx34.fonemas.muestras_audio{7};                      % extraigo "du"
snd{5}{1}.nombre = 'fdac1_sx34';                                            % nombre del archivo extra�do. Guardo para referencia
snd{5}{1}.nom_fonemas = 'du';                                               % Nombre del sonido que busco procesar
snd{5}{1}.valores = [rna(2).valor rna(5).valor];
snd{5}{1}.Fs = Fs;
textprogressbar(100);
textprogressbar('Finalizado');
%% ================== Proceso los sonidos a analizar : ====================

% En esta secci�n los sonidos a analizar son de la forma /cv/, es decir,
% consonante seguido por una vocal en una pronunciaci�n flu�da. Una vez
% extra�dos los sonidos de la base de datos, se guardan en forma separada,
% pero para analizarlos se unen los 2 sonidos en un solo array. 
   
    textprogressbar('Generando libros procesados           ');
    
path = 'K:\IIA\archivos de salida\';                                        % Ruta donde guardo archivos de salida
archivo_wav = [path 'wav1.dat'];
cant_snd = length(snd);                                                     % guardo la cantidad de sonidos a analizar


archivo_bib = [path 'pron_dV.bib'];                                         % lista de archivos con la pronunciacion dV
fid_bib = fopen (archivo_bib, 'wt');                                        % Abro para guardar texto (nombres de archivos)

for i = 1:cant_snd                                                          % Analizo todos los sonidos
    
    cant_ent = length(snd{i});                                              % cuento cant. de sonidos de este tipo 
    
    for j = 1 : 1                
       
        textprogressbar((i* 100)/cant_snd)
        
        lib_proc = Procesar_Pronunciacion(snd{i}{j}, archivo_wav); % Proceso el sonido elegido

        archivo_lib = [path snd{i}{j}.nombre '_' snd{i}{j}.nom_fonemas '.lbr'];                       % Archivo donde guardo el libro a procesar
       
        fprintf(fid_bib, '%s\n', archivo_lib);
        
        Guardar_Pronunciacion_Procesada (lib_proc, archivo_lib, snd{i}{j}.nom_fonemas );            % GUARDO libro de procesamineto para entrenar la red        

    end 
    
end
    textprogressbar('Finalizado');

fclose (fid_bib);                                                           % Cierro biblioteca de sonidos

fprintf(1, '\n\n');
disp ('Se finaliz� la escritura de libros de procesamiento');
