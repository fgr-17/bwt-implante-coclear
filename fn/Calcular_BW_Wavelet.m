%%=========================================================================================================================================
%
%>  @file           Calcular_BW_Wavelet.m
%
%>  @brief          Calculo el BW de una ondita
%
%>  @paramin        w       array de muestras de la ondita a analizar en fc del tiempo
%
%>  @author        Roux, Federico G.(rouxfederico@gmail.com)
%>  @company       RT-DSP UTN.BA
% =========================================================================================================================================

function [fci fcs] = Calcular_BW_Wavelet(h)


H = abs(fft(h));                                                            % Calculo el espectro de la Wavelet
L = length(H);                                                              % Calculo el largo del espectro de la wavelet

[Hmax iHmax] = max(H);                                                      % Calculo el maximo y su posici�n

Hc = Hmax / sqrt(2);                                                        % Caida en la frecuencia de corte

Hd = abs(H - Hc);                                                           % diferencia entre la respuesta del filtro y la frec de corte
Hd1 = Hd (1 : iHmax);                                                       % diferencia desde el origen hasta el m�ximo

Hd2 = Hd (iHmax: fix(L / 2));                                                  % diferencia desde el m�ximo hasta la mitad

[t1 iHc1] = min(Hd1);                                                       % Calculo el m�nimo de la diferencia entre 1 y el m�x (primer fcorte)
[t2 iHc2] = min(Hd2);                                                       % Calculo el m�nimo de la diferencia entre el max y L/2 (2da fcorte)

iHc2 = iHc2 + iHmax - 1;

fci = iHc1 / L;                                                             % frecuencia de corte inferior normalizada a Fs
fcs = iHc2 / L;                                                             % Frecuencia de corte superior normalizada a Fs
end
