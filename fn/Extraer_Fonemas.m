%% ========================================================================
%
%>  @file   Extraer_Fonemas.m
%
%>  @brief  extrae los fonemas de un archivo de la base de datos TIMIT para
%>  @brief  para guardarlos en una estructura
%
%>  @author Roux, Federico G. (rouxfederico@gmail.com)
%>  @company    UTN.BA 2014
%% ========================================================================

function entrada = Extraer_Fonemas (nombre_sin_extension)

    %% Creo diferentes nombres de archivo para cada una de las extensiones :
    archivo_wrd = [nombre_sin_extension '.WRD'];
    archivo_phn = [nombre_sin_extension '.PHN'];
    archivo_wav = [nombre_sin_extension '.WAV'];

    %% Abro el archivo de divisiones por palabra :

    [p_wrd, f_wrd, nom_wrd] = textread(archivo_wrd,'%n %n %s');

    N_wrd = length(nom_wrd);                                                    % Cantidad de palabras de la frase analizada  
    
    entrada.palabras.cantidad = N_wrd;                                          % Cantidad de palabras en la frase
    entrada.palabras.nombre = nom_wrd;                                          % Nombres de cada palabra 
    entrada.palabras.principio = p_wrd + 1;                                     % Numero de muestra de inicio de cada palabra
    entrada.palabras.final = f_wrd + 1;                                         % Numero de muestra de final de cada palabra
    entrada.palabras.largo = f_wrd - p_wrd;                                     % cantidad de muestras de audio de cada palabra
    
    kdesp = 380;
    entrada.palabras.principio = entrada.palabras.principio + kdesp;
    entrada.palabras.final = entrada.palabras.final + kdesp;

    %% Abro el archivo de divisiones por fonemas :
    [p_phn, f_phn, nom_phn] = textread(archivo_phn,'%n %n %s');

    N_phn = length(nom_phn);                                                    % Cantidad de fonemas de la base analizada

    entrada.fonemas.cantidad = N_phn;
    entrada.fonemas.nombre = nom_phn;
    entrada.fonemas.principio = p_phn + 1;
    entrada.fonemas.final = f_phn + 1;
    entrada.fonemas.largo = f_phn - p_phn;                                      % cantidad de muestras de audio de cada palabra

    kdesp = 380;
    entrada.fonemas.principio = entrada.fonemas.principio + kdesp;
    entrada.fonemas.final = entrada.fonemas.final + kdesp;
    
    %% Separo los sonidos del archivo :

    arch_texto = fileread(archivo_wav);                                         % abro el archivo como texto para quitar el encabezado
    pos_fin_enc  = strfind(arch_texto, 'end_head');                             % busco la cadena 'end_head' que indica el fin del encabezado
    largo_fin_enc = length('end_head');                                         % calculo el largo del fin del encabezado que es fijo
    
    fid = fopen(archivo_wav);                                                   % abro el archivo de las muestras de audio
    fseek(fid, pos_fin_enc + largo_fin_enc + 1, 'bof');                         % ubico el cursor del arhcivo despu�s del encabezado
    muestras_audio = fread (fid, inf, 'int16');                                 % leo todas las muestras de audio

    for i = 1:entrada.palabras.cantidad   
        entrada.palabras.muestras_audio {i} = muestras_audio (entrada.palabras.principio(i):entrada.palabras.final(i));    
    end

    for i = 1:entrada.fonemas.cantidad
        entrada.fonemas.muestras_audio {i} = muestras_audio (entrada.fonemas.principio(i):entrada.fonemas.final(i));
    end

end