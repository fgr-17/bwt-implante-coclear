%% ========================================================================
%
%>  @file   script_Entrenar_RNA_1.m
%
%>  @brief  Abro una biblioteca y extraigo los fonemas ya procesados para
%>  @brief  entrenar la RNA
%
%>  @author Roux, Federico G. (rouxfederico@gmail.com)
%>  @company    UTN.BA 2014
%% ========================================================================

%% ==================== Inicialización del script ========================= 


clear all;
fclose all;
close all 
clc;
disp(' ==============================================================');
disp(' -              PREPARACION DEL SET DE DATOS                  -');
disp(' ==============================================================');
fprintf(1, '\n\n');
%% Abro archivo de biblioteca :7

path_propio = mfilename('fullpath');                                        % Obtengo el path propio sin extension
path_desde_base = 'fn\script_Entrenar_RNA_1';                               % Path fijo del archivo en la base
path_base = strrep(path_propio, path_desde_base, '');                       % Extraigo path de la base


path_bib = [path_base 'archivos de salida\FPB\'];                              % Ruta donde guardo archivos de salida
archivo_bib = [path_bib 'FAKS0.bib'];                                       % lista de archivos con la pronunciacion dV
fid_bib = fopen (archivo_bib, 'rt');                                        % Abro para guardar texto (nombres de archivos)

i_libro = 1;

entrada = null(1);
target = null(1);

textprogressbar('Abriendo libros procesados           ');
%% Extraigo archivos de la biblioteca :


while(not(feof(fid_bib)))
    
    textprogressbar(10);
    archivo_lbr = fgets(fid_bib);                                                           % Leo una linea : corresponde a un archivo de libro
    archivo_lbr = [path_base archivo_lbr(1:(length(archivo_lbr) - 1))];                                 % Le quito el caracter de ENTER
    [libro_proc{i_libro}, nombre_fonema] = Abrir_Pronunciacion_Procesada (archivo_lbr);     % Abro el archivo y extraigo la información
    
    textprogressbar(40);
    array_libro = Libro_a_Array (libro_proc{i_libro});
    
    entrada = horzcat(entrada, array_libro);
    target = horzcat(target, libro_proc{i_libro}.etiq');
    i_libro = i_libro + 1;                                                                  % paso al siguiente libro
    textprogressbar(100);
end
fclose(fid_bib);
textprogressbar('Finalizado');

%% Inicializo la red previo a entrenarla :
fprintf(1, '\n\n\n');
disp(' ==============================================================');
disp(' -                   ENTRENAMIENTO DE LA RED                  -');
disp(' ==============================================================');

fprintf(1, ['\n *** Entrenando con set guardado en ' path_bib]);
fprintf(1, '\n\n');



for neuronas_CO = 20:5:100

    for repeticion = 1:5
    
        net = feedforwardnet(neuronas_CO);
        %net.efficiency.memoryReduction = 2;
        fprintf(1, '- Entrenando con %d neuronas CO. Vuelta %d\n', neuronas_CO, repeticion);
        %net = newff(entrada, target, 12);
        net.trainFcn = 'trainrp';
        net.trainParam.max_fail = 35;
        net = configure (net, entrada, target);

        
        %net.divideFcn = 'divideint';
        %net.layers{1}.transferFcn = 'purelin';
        % net.layers{2}.transferFcn = 'tansig';
        % net.layers{1}.dimensions = 11;

        % net.divideParam.trainRatio = 0.3;
        % net.divideParam.valRatio = 0.05;
        % net.divideParam.testRatio = 0.2;

        


        %% Genero la red y guardo la configuración en un archivo de texto :

        carpeta_salida = 'Redes_Entrenadas\FPB_RP\';

        nombre_sesion = sprintf('1_CO%d_%d',neuronas_CO, repeticion);
        path_salida = [path_base carpeta_salida nombre_sesion];

        mkdir(path_salida);

        archivo_info = [path_salida '\info_red.txt'];
        fid_info = fopen(archivo_info, 'wt');
        fprintf(fid_info, 'Informacion de entrenamiento de la red : \n\n');
        fprintf(fid_info, '\n\t> Tipo de red : %s', net.name);
        fprintf(fid_info, '\n\t> Cantidad de entradas : %d', net.numInputs);
        fprintf(fid_info, '\n\t> Cantidad de neuronas ocultas : %d', net.layers{1}.size);
        fprintf(fid_info, '\n\t> Funcion transf. capa oculta : %s', net.layers{1}.transferFcn);
        fprintf(fid_info, '\n\t> Funcion transf. capa salida : %s', net.layers{2}.transferFcn);
        fprintf(fid_info, '\n\t> Funcion de entrenamiento : %s', net.trainFcn);
        fprintf(fid_info, '\n\t> Cant. maxima de errores de validacion : %d', net.trainParam.max_fail);
        fclose(fid_info);

        %% Entreno la red con los parametros anteriores :
        fprintf(1, '\t> COMIENZA ENTRENAMIENTO\n');
        % net.trainParam.showWindow = false;
        % net.trainParam.showCommandLine = true;
        net = init(net);
        [net tr] = train(net, entrada, target);

        %% Guardo red y graficos de salida :

        output = net(entrada);                                                      % Simulo la salida para el entrenamiento hecho
        % output = sim(net, entrada);
        output_train = output(tr.trainInd);                                         % Salida del set de entrenamiento
        output_val = output(tr.valInd);                                             % Salida del set de validacion
        output_test = output(tr.testInd);                                           % Salida del set de testeo

        target_train = target(tr.trainInd);                                         % Targets del set de entrenamiento
        target_val = target(tr.valInd);                                             % Targets del set de validacion
        target_test = target(tr.testInd);                                           % Targets del set de testeo

        errores = gsubtract(target,output);
        % errores = target - output;
        performance = perform(net,target,output);
        
        regresion_train = regression(target_train, output_train);
        regresion_val = regression(target_val, output_val);
        regresion_test = regression(target_test, output_test);
        regresion_total = regression(target, output);
        
        fprintf(1, '\t> Resultados de la sesion :\n');
        fprintf(1, '\t\t - Regresion Entrenamiento : %f\n', regresion_train);
        fprintf(1, '\t\t - Regresion Validacion : %f\n', regresion_val);
        fprintf(1, '\t\t - Regresion Test : %f\n', regresion_test);
        fprintf(1, '\t\t - Regresion Total : %f\n', regresion_total);
        
        fig = plotregression(target_train, output_train, 'Entrenamiento', target_val, output_val, 'Validacion', target_test, output_test, 'Test', target, output, 'Todos');
        saveas(fig, [path_salida '\plot_regresion.png']);

        fig2 = plotperform(tr);
        saveas(fig2, [path_salida '\plot_performance.png']);

        % fig3 = ploterrhist(errores);
        % saveas(fig3, [path_salida '\plot_histograma.png']);

        % fig4 = plottrainstate(tr);
        % saveas(fig4, [path_salida '\plot_estado_train.png']);

        %% GUARDO GRAFICO DE LA RED PARA EL INFORME :

        jframe = view(net);

        %# create it in a MATLAB figure
        hFig = figure('Menubar','none', 'Position',[100 100 565 166]);
        jpanel = get(jframe,'ContentPane');
        [~, h] = javacomponent(jpanel);
        set(h, 'units','normalized', 'position',[0 0 1 1])

        %# close java window
        jframe.setVisible(false);
        jframe.dispose();

        %# print to file
        set(hFig, 'PaperPositionMode', 'auto')
        saveas(hFig, [path_salida '\diagrama_red.png'])

        %# close figure
        close all;

        save([path_salida '\red.mat'], 'net');

        %% Finalizo el script :
        close all;
        save([path_salida '\net.mat'], 'net');
        save([path_salida '\tr.mat'], 'tr');

        fprintf(1, '\t> Entrenamiento de la red finalizado\n\n');
    end
end