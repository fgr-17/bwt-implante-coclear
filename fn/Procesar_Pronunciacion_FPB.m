%% ========================================================================
%
%>  @file Procesar_Pronunciacion.m
%>  @function [y] = Procesar_Pronunciacion 
%>  @brief version propia del algoritmo de CWT, haciendo la convolucion de 
%>  @brief la senal con la integral de la wavelet desplazada y escalada y 
%>  @brief luego diferenciando esta seal (con el m�todo contrario usado 
%>  @brief para integrar).
%
%>  @author  ROUX, Federico G. (rouxfederico@gmail.com)
%
%>  @company    Profesor: Ing. BRUNO, Julian S.
%>  @company    Procesamiento Digital de Se�ales(PDS)
%>  @company    Departamento de Ing.Electronica.
%>  @company    Facultad Buenos Aires
%>  @company    Universidad Tecnologica Nacional
%% ========================================================================

% El an�lisis se hace en buffer de tama�o de 4ms, como est� en el paper, y
% se solapan algunas pocas muestras (15 de las 64 que tiene el buffer) para
% procesar de una manera m�s cont�nua. 

function libro_procesado = Procesar_Pronunciacion_FPB (snd, archivo_wavelet)

%% Inicializaci�n de valores :
UMBRAL_RMS = 2;
Fs_sonido = snd.Fs;                                                         % Frecuencia de muestreo del sonido a procesar
cant_fonemas = length(snd.s);                                               % Cantidad de fonemas contenidos

sonido = null(1);                                                                 % Inicializo array de sonidos
L = zeros(1, cant_fonemas);

ventana_dsp = 4e-3;                                                         % Ventana de procesamiento : 4ms
N_dsp = fix(ventana_dsp * Fs_sonido);                                       % cantidad de muestras a analizar por vez

ESCALAS_ELEGIDAS = 6;                                                       % Cantidad de escalas con mayor RMS elegidas por umbral 
N_SUP_PAPER = 10;                                                           % cantidad de muestras superpuestas en el paper
FS_PAPER = 11025;                                                           % Fs del paper como referencia
N_sup = fix(N_SUP_PAPER *(Fs_sonido / FS_PAPER));                           % Cantidad de muestras superpuestas reales

N_avance = N_dsp - N_sup;                                                   % cantidad de muestras que avanzo 

%% Formo una se�al de audio y mido los largos de cada porci�n. Saco media y ajusto rango :

for i_fonema = 1 : cant_fonemas                                             % Recorro todos los fonemas de la estructura
   sonido = [sonido ; snd.s{i_fonema}];                                     % Guardo los sonidos todos continuados
   L (i_fonema) = length(snd.s{i_fonema});   
end
%{
media_sonido = mean(sonido);
sonido = sonido - media_sonido;                                             % media cero

max_sonido = max(sonido);
min_sonido = min(sonido);
max_abs_sonido = max(abs(max_sonido), abs(min_sonido));
sonido = sonido / max_abs_sonido;                                           % Rango entre +1 y -1
%}
%% Abro FPB y calculo escalas

load FPB;

L_total = length(sonido);
y_fpb = zeros(ESCALAS_ELEGIDAS, L_total);
y_fpb(1, :) = filter(FPB_B1, 1, sonido);
y_fpb(2, :) = filter(FPB_B2, 1, sonido);
y_fpb(3, :) = filter(FPB_B3, 1, sonido);
y_fpb(4, :) = filter(FPB_B4, 1, sonido);
y_fpb(5, :) = filter(FPB_B5, 1, sonido);
y_fpb(6, :) = filter(FPB_B6, 1, sonido);


%% Calculo par�metros para la segmentaci�n de los sonidos :

cant_pag = floor((L - N_avance) ./ N_avance);                                 % Cantidad de iteraciones (p�g. de procesamiento)

cant_pag_total = sum(cant_pag);

y = zeros(cant_pag_total, ESCALAS_ELEGIDAS, N_dsp);                               % Inicializo array de salida

etiquetas = zeros(cant_pag_total, 1);                                             % Inicializo cantidad de etiquetas


pag_final = 1;
for i_fonema = 1 : cant_fonemas

  
    n_ini = 1;                                                                  % Inicializo nro. de muestra de origen de la ventana
    for pag = 1 : cant_pag(i_fonema)                                                      % Recorro todas las p�ginas que voy a generar
    
        
        % x = snd.s{i_fonema}(n_ini:(n_ini + N_dsp - 1));                              % porcion de sonido a procesar
       
        y(pag_final, :, :) = y_fpb(:, (n_ini:(n_ini + N_dsp - 1)));
        y_rms_total = sqrt(sum((y(pag_final, :,:) .^2), 3)/ N_dsp);
        y_rms_total = sqrt(sum(y_rms_total .^2)/ ESCALAS_ELEGIDAS);
        
        if(y_rms_total < UMBRAL_RMS)
            etiquetas(pag_final) = 0;
        else
            etiquetas(pag_final) = snd.valores (i_fonema);
        end
        
        pag_final = pag_final + 1;
        n_ini = n_ini + N_avance;
    end
    
end
libro_procesado.libro = y;
libro_procesado.etiq = etiquetas;
libro_procesado.n_dsp = N_dsp;
libro_procesado.n_sup = N_sup;

end