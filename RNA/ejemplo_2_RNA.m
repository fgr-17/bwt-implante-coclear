%% ========================================================================
%
%>  \file ejemplo_2_RNA.m
%
%>  \brief ejemplo de la secci�n "TRAINING STYLES"
%
%>  \author ROUX, Federico G. (rouxfederico@gmail.com)
%>  \company UTN.BA 2015
% ========================================================================

%% Inicializaci�n del script

close all;
clear all;
clc;

%% INCREMENTAL TRAINING :
%% Preparo el set de datos : quiero copiar la fc t = 2*p1 + p2;

P = {[1;2] [2;1] [2;3] [3;1]};                                              % Valores de entrada
T = {4 5 7 7};                                                              % Salida de la funci�n


%% Inicializaci�n de la RNA : 

net = linearlayer(0,0);                                                     % Inicializo capa en cero
net = configure(net,P,T);                                                   % Configuro cantidad de entradas y salidas de acuerdo al set de datos
net.IW{1,1} = [0 0];                                                        % Inicializo los pesos en cero
net.b{1} = 0;                                                               % Inicializo Bias en cero

%% ENTRENAMIENTO DE LA RED : 

[net, a, e, pf] = adapt (net, P, T);                                        % no hace nada porque el "learning rate" est� en cero

net.inputWeights{1}.learnParam.lr = 0.1;                                    % Qu� medida tengo para este par�metro?
net.biases{1}.learnParam.lr = 0.1;

[net, a, e, pf] = adapt (net, P, T);                                        % Ahora si la entrena con una LR = 0.1

%% Muestro resultado por consola :

fprintf ('==============================================================');
fprintf(1, '\n\nEntrenamiento Incremental\n\n');
fprintf(1, 'a = \n');
disp(a);
fprintf(1, 'e = \n');
disp(e);

%% BATCH TRAINING :
%% Preparo el set de datos :

P = [1 2 2 3; 2 1 3 1];                                                     % Meto los datos en una sola matriz para este tipo de entrenamiento
T = [4 5 7 7];

%% Inicializo la red :

net = linearlayer(0,0.01);                                                  % Ajusto de entrada el LR = 0.01
net = configure(net,P,T);
net.IW{1,1} = [0 0];
net.b{1} = 0;

%% Entreno la red con 'adapt()'

[net,a,e,pf] = adapt(net,P,T);                                              % Entreno la red. 'a' y 'e' quedaran en cero y m�x respectivamente

%% Muestro resultado por consola :

fprintf ('==============================================================');
fprintf(1, '\n\nEntrenamiento Batch con adapt()\n\n');
fprintf(1, 'a = \n');
disp(a);
fprintf(1, 'e = \n');
disp(e);

%% ENTRENAMIENTO CON 'train()': SIEMPRE BATCH MODE :
%% Preparo el set de datos

P = [1 2 2 3; 2 1 3 1];                                                     % Siempre convierte 
T = [4 5 7 7];

%% Inicializo la red :

net = linearlayer(0,0.01);
net = configure(net,P,T);
net.IW{1,1} = [0 0];
net.b{1} = 0;

%% Entreno usando 'train()':

% net.trainParam.showWindow = false;                                        % No abro ventana de la GUI
% net.trainParam.showCommandLine = true;                                    % Salida por command line
% net.trainParam.show= 35;                                                  % Actualizo cada 35 epocas

net.trainParam.epochs = 1;                                                  % Cantidad de pasadas que hago de los datos de entrada
net = train(net, P, T);                                                     % Entreno la red con la funcion 'train' usando algoritmo Widrow-Hoff

%% Muestro resultado por consola :

fprintf ('==============================================================');
fprintf(1, '\n\nEntrenamiento con train() (BATCH)\n');
fprintf(1, 'Pesos [W] :');
disp(net.IW{1, 1});
fprintf(1, 'Bias [b] :');
disp(net.b{1});






