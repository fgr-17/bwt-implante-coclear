%% ========================================================================
%
%>  \file prueba_RNA.m
%
%>  \brief script de prueba del ejemplo de matlab sobre entrenamiento de
%>  \brief una RNA de 2 capas entrenada con backprop.
%
%>  \author ROUX, Federico G. (rouxfederico@gmail.com)
%>  \company UTN.BA 2015
% ========================================================================


%% Inicialización del script

close all;
clear all;
clc;

%% Inicialización de la RNA : 

net = feedforwardnet;                                                       % Genero la estructura por default con esta función

net.layers{1}.dimensions = 20;                                              % Cambio la dimensión a 20 para quedar igual que en el ejemplo

%% Inicialización del set de datos

p = -2:.1:2;                                                                % Array temporal, datos de entrada
t = sin(pi*p/2);                                                            % Función de salida, quiero mapear esta función ??

%% Configuro la red con la función "configure()" :

net1 = configure(net,p,t);
