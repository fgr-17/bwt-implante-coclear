%% ========================================================================
%
%>  \file grafico_regresion.m
%
%>  \brief ejemplo de la sección "Post-Training Analysis"
%
%>  \author ROUX, Federico G. (rouxfederico@gmail.com)
%>  \company UTN.BA 2015
% ========================================================================

%% Inicialización del script

close all;
clear all;
clc;

%% Set de datos y entrenamiento de la red :

load house_dataset
net = feedforwardnet;

net.trainParam.showWindow = false;
net.trainParam.showCommandLine = true;
[net, tr] = train(net, houseInputs, houseTargets);

houseOutputs = net(houseInputs);                                            % calculo salida de la red al set de datos

trOut = houseOutputs(tr.trainInd);                                          % extraigo datos del set de train
vOut = houseOutputs(tr.valInd);                                             % extraigo datos del set de validacion
tsOut = houseOutputs(tr.testInd);                                           % extraigo datos del set de test

trTarg = houseTargets(tr.trainInd);                                         % extraigo salidas separadas por cada parte del set 
vTarg = houseTargets(tr.valInd);
tsTarg = houseTargets(tr.testInd);

plotregression(trTarg,trOut,'Train',vTarg,vOut,'Validation',...
tsTarg,tsOut,'Testing')